import { Router, Request, Response, NextFunction } from "express";
import { UsersController } from "./../controller/Userscontroller";
const UserRouter: Router = Router();

// http://localhost:8000/api/users
UserRouter.route("/")
  // GET
  .get(async (req: Request, res: Response, next: NextFunction) => {
    const idUser: string = req.query.id?.toString() || "";
    const controller: UsersController = new UsersController();
    const response = await controller.getUsers(idUser);
    if (!response) return next("user no found");
    res.json(response);
  })
  // DELETE
  .delete(async (req: Request, res: Response, next: NextFunction) => {
    const idUser: string = req.query.id?.toString() || "";
    const controller: UsersController = new UsersController();
    const response = await controller.removeUserById(idUser);
    if (!response) return next("user no found");
    res.json(response);
  })
  // POST
  .post(async (req: Request, res: Response) => {
    const { name, lastname, age, email } = req.body;

    const controller = new UsersController();
    const response = await controller.createNewUser({
      name,
      lastname,
      age,
      email,
    });

    res.send(response);
  })
  // PUT
  .put(async (req: Request, res: Response) => {
    const controller = new UsersController();
    const { name, lasname, age } = req.body;
    const id: any = req.query?.id;
    const response = await controller.updateUserbyId(id, {
      name,
      lasname,
      age,
    });
    res.send(response);
  });

export default UserRouter;
