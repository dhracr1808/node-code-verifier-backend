/**
 * Root Router
 * Redictions Routers
 */

import express, { Errback, NextFunction, Request, Response } from "express";
import helloRouter from "./HelloRouter";
import userRouter from "./UserRouter";
import { logInfo } from "../utils/logger";

// Server instance
let server = express();

// Router instance
let rootRouter = express.Router();

// Activate  for Resquest to http://localhost:8000/api

// GET http://localhost:8000/api
rootRouter.get("/", (req: Request, res: Response) => {
  logInfo("GET: http://localhost:8000/api");
  res.send("typescript + node + express");
});

// Redirections to Routers & controllers
server.use("/", rootRouter);
server.use("/hello", helloRouter);

// add more routes to the app
server.use("/users", userRouter); //http://localhost:8000/api/users -> UserRouer;

server.use("/*", (req: Request, res: Response) => {
  try {
    res.json({ message: "route no found" });
  } catch (error) {
    res.sendStatus(500);
  }
}); //http://localhost:8000/api/users -> UserRouer;

server.use(
  (error: Errback, req: Request, res: Response, next: NextFunction) => {
    try {
      res.status(404).json({ message: error, status: res.statusCode });
    } catch (err) {
      res.sendStatus(500);
    }
  }
); //http://localhost:8000/api/users -> UserRouer;

export default server;
