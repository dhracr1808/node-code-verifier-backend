import { Request, Response, Router } from "express";
import { logInfo } from "../utils/logger";
import { HelloController } from "../controller/HelloController";
import { BasicResponse } from "../controller/types";

// Router from express;
const helloRouter = Router();

//GET -> http://localhost:8000/api/hello?name=ejemplo
helloRouter.route("/").get(async (req: Request, res: Response) => {
  // obtain a Query param
  let name: any = req?.query?.name;
  logInfo(`Query param: ${name}`);

  // Controller instance to execute  method
  const controller: HelloController = new HelloController();

  // Response and send Server
  res.send(await controller.getMessage(name) as BasicResponse);
});

export default helloRouter;
