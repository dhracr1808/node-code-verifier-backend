import mongoose from "mongoose";

export const userEntity = () => {
  const userSchema = new mongoose.Schema({
    name: { type: String, unique: true },

    lastname: String,
    email: { type: String, required: true, unique: true },
    age: Number,
  });

  return mongoose.models.Users || mongoose.model("Users", userSchema);
};
