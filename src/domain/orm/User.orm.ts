import { userEntity } from "../entitis/User.entity";
import { logError, logSuccess } from "../../utils/logger";

// CRUD

/**
 * Method to obtain all Users from Collection "Users"  in Mongo Sever
 */

export const getAllUsers = async (): Promise<Array<any> | undefined> => {
  try {
    let userModel = userEntity();
    // Search all Users
    return await userModel.find({ isDelete: false });
  } catch (error) {
    logError(`[ORM ERROR]: Getting All Users  ${error}`);
  }
};

// - Get User By ID

export const getUserById = async (id: string): Promise<any | undefined> => {
  try {
    const userModel = userEntity();
    // Search User by ID
    return await userModel.findById(id);
  } catch (error) {
    logError(`[ORM ERROR]: Getting  User By Id  ${error}`);
  }
};

// - Delete User By ID

export const deleteUserById = async (id: string): Promise<any> => {
  try {
    const userModel = userEntity();
    // Search User by ID
    return await userModel.deleteOne({ _id: id });
  } catch (error) {
    logError(`[ORM ERROR]: Removeing  User By Id  ${error}`);
  }
};

// - Create New User

export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    const userModel = userEntity();
    return await userModel.create(user);
  } catch (error) {
    logError(`[ORM ERROR]: Creating User:  ${error}`);
  }
};

// - Update User By ID

export const updateUser = async (id: string, user: any): Promise<any> => {
  try {
    const userModel = userEntity();
    return await userModel.findByIdAndUpdate(id, user);
  } catch (error) {
    logError(`[ORM ERROR]: Updating User:  ${error}`);
  }
};

// TODO:
// - Get User By Email
