import express, { Express, Request, Response } from "express";

// Swagger
import swaggerUi from "swagger-ui-express";

//security
import cors from "cors";
import helmet from "helmet";

// TODO: HTTPS

// Root Router
import routes from "../routes";
import mongoose from "mongoose";

const server: Express = express();

// * swagger config and route

server.use(
  "/docs",
  swaggerUi.serve,
  swaggerUi.setup(undefined, {
    swaggerOptions: {
      url: "/swagger.json",
      explorer: true,
    },
  })
);

// * Json
server.use(express.urlencoded({ extended: true }));
server.use(express.json());

// Define SERVER  to use     "/api" and use
server.use("/api", routes);

// static Server
server.use(express.static("public"));

// TODO:: Mongo connection

mongoose.connect("mongodb://localhost:27017/codeverification");

// Security config

server.use(cors());
server.use(helmet());

// content Type:

server.use(express.urlencoded({ extended: true, limit: "500mb" }));
server.use(express.json({ limit: "500mb" }));

// http:localhost:8000/ ->  http:localhost:8000/api
server.get("/", (req: Request, res: Response) => {
  res.redirect("/api");
});

export default server;
