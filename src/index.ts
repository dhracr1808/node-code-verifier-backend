import server from "./server";
import { logError, logSuccess } from "./utils/logger";
//environment variables
import dotenv from "dotenv";

/* ================= inicial dotenv ================= */
dotenv.config();

const PORT: string | number = process.env.PORT || 5000;

/* ================= Execute Server  ================= */
server.listen(PORT, () => {
  logSuccess(`[SERVER ON]: Running in http://localhost:${PORT}/api`);
});

// * Control Server Error

server.on("error", (error) => {
  logError(`[SERVER ERROR]: ${error}`);
});
