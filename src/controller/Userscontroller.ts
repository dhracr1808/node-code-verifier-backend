import { Get, Route, Tags, Query, Delete, Post, Put } from "tsoa";
import { IUsersController } from "./interfaces";
import { logSuccess, logError, logWarning } from "../utils/logger";

// ORM - Users Controller
import {
  getAllUsers,
  getUserById,
  deleteUserById,
  createUser,
  updateUser,
} from "./../domain/orm/User.orm";

@Route("api/users")
@Tags("usersController")
export class UsersController implements IUsersController {
  /**
   *@param {string} id Id of user to retreive (optional)
   *@returns all user o user by ID
   */
  @Get("/")
  public async getUsers(@Query() id?: string): Promise<any> {
    if (!id) {
      logSuccess("[/api/users] Get all Users Request");
      return await getAllUsers();
    } else {
      logSuccess(`[/api/users/id] Get  User by ID: ${id}`);
      return await getUserById(id);
    }
  }

  /**
   * endpoind to delete the Users in the collection "Users" of DB
   *@param {string} id Id of user to delete (Optional)
   *@returns  message informing if deletion was correct
   */
  @Delete("/")
  public async removeUserById(@Query() id?: string): Promise<any> {
    if (id) {
      logSuccess(`[/api/users/id] remove User by ID: ${id}`);
      return await deleteUserById(id);
    } else {
      logWarning(`[/api/users] Delete User  Request WITHOUT ID`);
      return {
        message: "please, provide an ID to remove from data base",
      };
    }
  }

  /**
   *of user to retreive (optional)
   *@returns all user o user by ID
   */
  @Post("/")
  public async createNewUser(user: any) {
    logSuccess("[/api/users] Post user create");
    return await createUser(user);
  }

  @Put("/")
  public async updateUserbyId(id: string, user: any) {
    return await updateUser(id, user).then((res) => {
      return { message: `${id} updating success` };
    });
  }
}
