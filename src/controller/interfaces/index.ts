import { BasicResponse } from "controller/types";

export interface IHelloController {
  getMessage(): Promise<BasicResponse>;
}

export interface IUsersController {
  // Read all users fron database
  getUsers(id: string): Promise<any>;

  // find user by ID (objecjID) and removing
  removeUserById(id: string): Promise<any>;
  // find user by ID (objecjID)

  //getUserById(id: string | number): Promise<any>;

  // create new user
  createNewUser(user: any): Promise<any>;

  // updating user
  updateUserbyId(id: string, user: any): Promise<any>;
}
